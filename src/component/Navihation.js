import React, { Component } from 'react';
import Links from './navihationcomponents/Links';

class Navihation extends Component {
    state = {
        visibility: true
    }

    componentDidMount(){
        const local = window.location.pathname;
        let initPoint = "";
        if(local === '/agent' || local === '/client'){
            localStorage.setItem('local', local)
            initPoint = localStorage.getItem('local');
        }else{
            localStorage.removeItem('local')
            initPoint = localStorage.getItem('local');
        }
        if(initPoint !== null){
            this.setState({
                visibility: false
            })
        }
    }

    closed = () => {
        this.setState({
            visibility: false
        })
    }
    
    render() {
        let { visibility } = this.state;
        return (
            <>
                <div id="popStart" className={!visibility  ? 'hidden' : ''}>
                    <div className="shadows">
                        <div className="strting__pop">
                            <Links closed={this.closed} />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Navihation;
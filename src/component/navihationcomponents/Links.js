import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Links extends Component {

    state = {
        data: [
            {
                parts: 'client__part',
                images: 'backgraund_client',
                linked: '/client',
                title: 'Создать страницу на B2C'
            },
            {
                parts: 'agent__part',
                images: 'backgraund_agent',
                linked: '/agent',
                title: 'Создать страницу на B2B'
            }
        ] 
    }
    render() {
        let { closed } = this.props;
        let { data } = this.state;
        return(
            <div className="pop__wrap">
                {
                    data.map(({parts, images, linked, title}, i) =>(
                        <div key={i} className={parts}>
                            <Link to={linked} className="link-wrap" onClick={closed}>
                                <div className={`${images} pop-img_b`}></div>
                                <div className="pop-title__text">{title}</div>
                            </Link>
                        </div>
                    ))
                }
            </div>
        );
    }
}

export default Links;
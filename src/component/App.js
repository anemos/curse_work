import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../redux/store'


import Header from './Header';
import Maine from './Maine';
import Navihation from './Navihation';
import '../style/App.css';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Header />
                <Navihation  />
                <Maine />
            </Provider>
        )
    }
};

export default App;

import React, { Component } from 'react';
import logoweb from '../images/logo-web_white.png';
import '../style/Header.css';


class Header extends Component {
    render() {
        return (
            <>
                <header className="compiler__header">
                    <div className="wrap realative-pos">
                        <div className="logo__web">
                            <a href="mailto:web@tui.ru"><img src={logoweb} className="img_logo" alt="web logo"/></a>
                        </div>
                    </div>
                </header> 
            </>
        )
    }
}

export default Header;

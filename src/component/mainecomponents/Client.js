import React, { Component } from 'react';
import { dataClient } from '../../datacommon/client'
import ConnectedData from '../../container/ConnectedData'
import HookData from '../../container/HookData'

class Client extends Component {
    render() {
        return (
            <>
                <h1>Hello Client</h1>
                <ConnectedData/>
                <HookData base={ dataClient }/>
            </>
        )
    }
}

export default Client;
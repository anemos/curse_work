import React, { Component } from 'react';
import ConnectedData from '../../container/ConnectedData'
import HookData from '../../container/HookData'
import { dataAgent } from '../../datacommon/agent'

class Agent extends Component {
    render() {
        return (
            <>
                <h1>Hello Agent</h1>
                <ConnectedData/>
                <HookData base={ dataAgent }/>
            </>
        )
    }
}

export default Agent;

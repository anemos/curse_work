import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { titles } from '../../helpers/name.list'

import { chengePart } from '../../actions/actions';

const Input = React.memo(({ name, removeHendler, uniqId }) => {
    const [value, setValue] = useState('');
    const [uniq] = useState(uniqId)
    const [position, setPosition] = useState(true);
    const [colors, setColors] = useState('green');

    const dispatch = useDispatch();

    const chenger = (event) => {
        setValue(event.target.value);
    };

    const uppdater = () => {
        if(value.length > 0){
            dispatch (chengePart(uniq, value));
            setPosition(false);
            if(position === false){
                setColors('orange');
                setTimeout(
                    function () {setColors('green')}
                , 500)
            }
        }else{
            alert(`Заполните поле: "${name}" пожалуйста`)
        }
    }

    return(
        <div>
            <div className="name__input">{
                titles.map(e => (
                    e.header === name ? name = e.titles : ''
                ))
            }</div>
            <div className="input__block">
                <input value={value} onChange={ chenger }/>
                {
                    position 
                        ? <button onClick={uppdater} className="chose__button">Добавить</button>
                        : <button onClick={uppdater} className='chose__button' style={{backgroundColor: colors, color: '#fff'}}>Обновить</button>
                }
                <button onClick={removeHendler(uniq)} className="chose__button">Удалить</button>
                
            </div>
        </div>
    )
})

Input.propTypes = {
    name: PropTypes.string,
    uniqId: PropTypes.string.isRequired,
    removeHendler: PropTypes.func.isRequired,
}

export default Input;
import React, { Component } from 'react'
import { FcMms, FcClock, FcDocument } from "react-icons/fc";
import { dataClient } from '../../datacommon/client'
import { dataAgent } from '../../datacommon/client'

import OneTools from './OneTools'
import Input from './Input'

class ToolsLists extends Component {
    callInput = (e) => _ =>{
        this.props.add('', e);
    }

    removeInput = (e) => _ => {
        this.props.remove(e);
    }
    
    render = () => {
        const { callInput, removeInput, compilation } = this;
        const { data } = this.props;
        return(
            <>
                <div className="chose__box">
                    <OneTools text="Главный баннер" hendler={callInput('banner')}>
                        <FcMms />
                    </OneTools>
                    <OneTools text="Период акции" hendler={callInput('date')}>
                        <FcClock/>
                    </OneTools>
                    <OneTools text="Краткий текст" hendler={callInput('text')}>
                        <FcDocument/>
                    </OneTools>
                </div>
                <div className="custom_fields">
                    {
                        data.length > 0 
                        ? data.map((e, index) => {
                            return <Input
                                key={e.id}
                                uniqId={e.id}
                                name={e.titles} 
                                removeHendler={removeInput}
                            />
                        })
                        : ''
                    }
                </div>
            </>
        )
    }
}

export default ToolsLists
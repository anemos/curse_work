import React from 'react'

const OneTools = ({children, text, hendler }) => {
    
    return(
        <div className="clicker" onClick={ hendler }>
            <div className="icon">
                { children }
            </div>
            <div className="clickr__text">
                { text }
            </div>
        </div>
    )
}

export default OneTools;
import React from 'react'

const CompilerBtn = ({ base, data }) => {

    const compilation = (item) => _ => {
        let pager = item[0].body;
        data.forEach(element => {
            item.forEach(fields => {
                if(element.titles === fields.titles){
                    pager += fields.body(element.body)
                }
            })
        });
        pager += item[item.length - 1].body
        let datas = new Blob([pager], {
            type: 'html'
        }),
        csvURL = window.URL.createObjectURL(datas),
        tempLink = document.createElement('a');
        tempLink.href = csvURL;
        tempLink.setAttribute('download', 'action.html');
        tempLink.click()
    }

    return(
        <>
            {
                data.length > 0
                ? <button className="compiler__btn" onClick={compilation(base, data)}>Скачать</button>
                : ''
            }
        </>
    )
}

export default CompilerBtn;
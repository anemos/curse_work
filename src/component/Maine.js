import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Agent from './mainecomponents/Agent';
import Client from './mainecomponents/Client';

class Maine extends Component {
    render() {
        return (
            <>
                <main className="maine__wrap">
                    <Switch>
                        <Route path='/client' component={ Client }/>
                        <Route path='/agent' component={ Agent }/>
                    </Switch>
                </main>
            </>
        )
    }
}

export default Maine;
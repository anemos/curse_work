import {
    ADD_PART,
    UPDATE_PART,
    REMOVE_PART
} from '../constants/constants';
import uniqid from 'uniqid';

export const addPart = ( value, header ) => ({ 
    type: ADD_PART, 
    payload: {
        id: uniqid('item-'),
        titles: header,
        body: value
    }
});
export const chengePart = ( id, value ) => ({ type: UPDATE_PART, payload: { uniq: id, body: value} });
export const removePart = ( index )  => ({ type: REMOVE_PART, payload: index })
import {
    ADD_PART,
    UPDATE_PART,
    REMOVE_PART,
} from '../constants/constants';

const initState = {
    data: []
}

const reducer = ( state = initState, action ) => {
    switch( action.type ){
        case ADD_PART:
            return({
                ...state,
                data: [ ...state.data, action.payload ]
            });
        
        case UPDATE_PART:
            return({
                ...state,
                data: state.data.map(item => {
                    if(item.id === action.payload.uniq){
                        item.body = action.payload.body;
                    }
                    return item;
                })
            })

        case REMOVE_PART:
            return({
                ...state,
                data: state.data.filter( item => item.id !== action.payload )
            }); 

        default:
            return state;

    }
}

export default reducer;
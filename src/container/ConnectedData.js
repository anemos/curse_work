import { connect } from 'react-redux';
import { removePart, addPart } from '../actions/actions';
import ToolsLists from '../component/tools/ToolsLists';

const mapStateToProps = (state) => {
    return { data: state.data }; 
};

const mapDispatchToProps = (dispatch) => ({
    remove: (e) => dispatch(removePart(e)),
    add: (value, header, uniq) => dispatch(addPart(value, header, uniq))
});

export default connect(mapStateToProps, mapDispatchToProps )(ToolsLists);

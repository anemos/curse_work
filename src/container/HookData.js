import React from 'react'
import { useSelector } from 'react-redux';
import { getDataSelector } from '../selector/selector';
import CompilerBtn from '../component/tools/CompilerBtn';

const HookData = ({ base }) => {
    const data = useSelector(getDataSelector);
    return(
        <CompilerBtn data={data} base={base}/>
    )
}

export default HookData;

export const dataAgent = [
    {
        index: 1,
        titles: `head`,
        body: ` 
        <!DOCTYPE html>
        <html>
            <head>
                <title>Привет Агент</title>
            </head>
        <body>
        `
    },
    {
        index: 2,
        titles: `banner`,
        body: (src) => {
            return `
            <div class="main__banner">
                <img class="desktop_img desktop" alt="main-banner" src="${src}">
            </div>
            `
        }
    },
    {
        index: 3,
        titles: `text`,
        body: (text) => {
            return `
            <div class="action__text">
                ${text}
            </div>
            `
        }
    },
    {
        index: 4,
        titles: `date`,
        body: (start) => {
            return `
                <div class="line_space">
                    <h3>Сроки акции</h3>
                    <ul class="squaredList">
                        <li><strong>Даты бронирования:</strong>${start}</li>
                    </ul>
                </div>
            `
        }
    },
    {
        index: 5,
        titles: `footer`,
        body: `
                </body>
            </html>
            `
    },
]